import {bigIntIsEqual} from "../../../../main/ts/isEqual/numericIsEqual/bigIntIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null];

const stringArray = ["first", "second", "third"];
const intArray = [3, 6, 9];
const mixedArray1 = ["first", 2, true, {}, Symbol("aSymbol")];
const mixedArray2 = [false, {foo: 123}, "bar", 456, BigInt(99999)];

const arrays = [stringArray, intArray, mixedArray1, mixedArray2];

const primitiveNumbers = [-432, -100, 0, 212, 99999, Infinity, NaN];

const bigInts = primitiveNumbers.filter(num => Number.isFinite(num)).map(BigInt);


const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);
const map2 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));
const map3 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const maps = [map1, map2, map3];

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const nonNumbers = [...objects, ...arrays, ...maps, new Set(mixedArray2), () => "", "foo", Symbol("aSymbol"), ...singletonPrimitives];

const nonBigInts = [...primitiveNumbers, ...nonNumbers];

const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

suite("bigIntIsEqual", function testBigIntIsEqual()
{
    suite("invalid source BigInt throws", function testInvalidSourceBigIntThrows()
    {
        nonBigInts.forEach(nonNumber =>
            test(`bigIntIsEqual(${toStr(nonNumber)}, 42) throws`, function()
            {
                assert.throws(() => bigIntIsEqual(nonNumber as unknown as BigInt, 42), TypeError);
            })
        );
    });

    suite("equality of non-number returns false", function testNonNumberReturnsFalse()
    {
        nonNumbers.forEach(nonNumber =>
            test(`bigIntIsEqual(BigInt(42), ${toStr(nonNumber)})`, function()
            {
                assert.isFalse(bigIntIsEqual(BigInt(42), nonNumber));
            })
        );
    });

    suite("primitive number singletons", function testEqualityOfPrimitiveNumericSingletons()
    {
        [Infinity, NaN].forEach(numericPrimitiveSingleton =>
            test(`bigIntIsEqual(BigInt(42), ${numericPrimitiveSingleton}) returns false`, function test42AndNumericPrimitiveSingletonReturnsFalse()
            {
                assert.isFalse(bigIntIsEqual(BigInt(42), numericPrimitiveSingleton));
            })
        );
    });

    suite("equal BigInt returns true", function testEqualBigIntReturnsTrue()
    {
        bigInts.forEach(bigInt =>
            test(`bigIntIsEqual(${toStr(bigInt)}, ${toStr(bigInt)}) returns true`, function()
            {
                assert.isTrue(bigIntIsEqual(bigInt, bigInt));
            })
        );
    });

    suite("equal primitive number returns true", function testEqualPrimitiveNumberReturnsTrue()
    {
        primitiveNumbers.forEach(number => {
            if (Number.isFinite(number))
            {
                test(`bigIntIsEqual(BigInt(${number}), ${number}) returns true`, function()
                {
                    assert.isTrue(bigIntIsEqual(BigInt(number), number));
                });
            }
        });
    });

    suite("unequal primitive number returns false", function testUnequalPrimitiveNumberReturnsFalse()
    {
        primitiveNumbers.forEach((number, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (Number.isFinite(number) && index !== altIndex)
            {
                test(`bigIntIsEqual(BigInt(${number}), ${arr[altIndex]}) returns false`, function()
                {
                    assert.isFalse(bigIntIsEqual(BigInt(number), arr[altIndex]));
                });
            }
        });
    });

    suite("unequal BigInt returns false", function testUnequalBigIntReturnsFalse()
    {
        bigInts.forEach((bigInt, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`bigIntIsEqual(BigInt(${bigInt}), ${arr[altIndex]}) returns false`, function()
                {
                    assert.isFalse(bigIntIsEqual(bigInt, arr[altIndex]));
                });
            }
        });
    });
});
