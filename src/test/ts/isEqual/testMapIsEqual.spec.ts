import {mapIsEqual} from "../../../main/ts/isEqual/mapIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null, Infinity, NaN];

const stringArray = ["first", "second", "third"];
const intArray = [3, 6, 9];
const mixedArray1 = ["first", 2, true, {}];
const mixedArray2 = [false, {foo: 123}, "bar", 456];

const arrays = [stringArray, intArray, mixedArray1, mixedArray2];

const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);

const map2 = new Map(mixedArray2.entries());

const map3 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));

const map4 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const maps = [new Map(), map1, map2, map3, map4];

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

const nonMaps = [...objects, ...arrays, new Set(mixedArray2), () => "", 123, "foo", BigInt(999999999), Symbol("aSymbol"), ...singletonPrimitives];

suite("mapIsEqual", function testMapIsEqual()
{
    suite("invalid source map throws", function testInvalidSourceMapThrows()
    {
        nonMaps.forEach(nonMap =>
            test(`mapIsEqual(${toStr(nonMap)}, Map()) throws`, function()
            {
                assert.throws(() => mapIsEqual(nonMap as unknown as Map<unknown, unknown>, maps[0]), TypeError);
            })
        );
    });

    suite("equality of non map returns false", function testNonMapReturnsFalse()
    {
        nonMaps.forEach(nonMap =>
            maps.forEach(map =>
                test(`mapIsEqual(${toStr(map)}, ${toStr(nonMap)}) returns false`, function()
                {
                    assert.isFalse(mapIsEqual(map, nonMap));
                }))
        );
    });

    suite("equality of unequal map returns false", function testUnequalMapReturnsFalse()
    {
        maps.forEach((map, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`mapIsEqual(${toStr(map)}, ${toStr(arr[altIndex])}) returns false`, function()
                {
                    assert.isFalse(mapIsEqual(map, arr[altIndex]));
                });
            }
        });
    });
});
