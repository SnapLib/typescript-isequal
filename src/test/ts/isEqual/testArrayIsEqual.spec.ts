import {arrayIsEqual} from "../../../main/ts/isEqual/arrayIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null, Infinity, NaN];

const strArr1 = ["first", "second", "third"];
const strArr2 = ["fourth", "fifth", "sixth"];

const intArr1 = [1, 2, 3];
const intArr2 = [4, 5, 6];

const mixedArray1 = ["first", 2, true, {}];
const mixedArray2 = [false, {foo: 123}, "bar", 456];

const strArrays = [strArr1, strArr2];
const intArrays = [intArr1, intArr2];
const mixedArrays = [mixedArray1, mixedArray2];

const arrays = [[], ...strArrays, ...intArrays, ...mixedArrays, singletonPrimitives];

const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);
const map2 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));
const map3 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const maps = [map1, map2, map3];

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const sets = arrays.map(array => new Set(array));


const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

const nonArrays = [...objects, () => "", ...sets, ...maps, 123, "foo", BigInt(999999999), Symbol("aSymbol"), ...singletonPrimitives];

suite("arrayIsEqual", function testArrayIsEqual()
{
    suite("invalid source array throws", function testInvalidSourceArrayThrows()
    {
        nonArrays.forEach(invalidArg =>
            test(`arrayIsEqual(${toStr(invalidArg)}, []) throws`, function()
            {
                assert.throws(() => arrayIsEqual(invalidArg as unknown as Array<unknown>, []), TypeError);
            })
        );
    });

    suite("equality of non array returns false", function testNonArraysReturnsFalse()
    {
        nonArrays.forEach(nonArray =>
            test(`arrayIsEqual(["first", "second", "third"], ${toStr(nonArray)}) returns false`, function()
            {
                assert.isFalse(arrayIsEqual(["first", "second", "third"], nonArray));
            })
        );

        test('arrayIsEqual(["first", "second", "third"], new Set(["first", "second", "third"])) returns false', function testArrayToSetIsFalse()
        {
            assert.isFalse(arrayIsEqual(["first", "second", "third"], new Set(["first", "second", "third"])));
        });
    });

    suite("equality of same array returns true", function testSameArrayReturnsTrue()
    {
        arrays.forEach(mockArray =>
            test(`arrayIsEqual(${toStr(mockArray)}, ${toStr(mockArray)}) returns true`, function()
            {
                assert.isTrue(arrayIsEqual(mockArray, mockArray));
            })
        );
    });

    suite("equality of structurally equal array returns true", function testSameStructureArrayReturnsTrue()
    {
        arrays.forEach(mockArray =>
            test(`arrayIsEqual(${toStr(mockArray)}, new Array(...${toStr(mockArray)})) returns true`, function()
            {
                assert.isTrue(arrayIsEqual(mockArray, new Array(...mockArray)));
            })
        );
    });

    suite("equality of unequal array returns false", function testUnequalArrayReturnsFalse()
    {
        arrays.forEach((mockArr, index, arr) => {
            const altIndex = arr.length - 1 - index;
            const altMockArr = arr[altIndex];

            if (index !== altIndex)
            {
                test(`arrayIsEqual(${toStr(mockArr)}, ${toStr(altMockArr)}) returns false`, function()
                {
                    assert.isFalse(arrayIsEqual(mockArr, altMockArr));
                });
            }

            test(`arrayIsEqual(${toStr(mockArr)}, ${toStr(altMockArr.reverse())}) returns false`, function()
            {
                assert.isFalse(arrayIsEqual(mockArr, altMockArr.reverse()));
            });
        });

        const arr1 = ["1", "2", "3"];

        test(`arrayIsEqual(["1", "2", "3"], ${toStr(intArr1)}) returns false`, function()
        {
            assert.isFalse(arrayIsEqual(arr1, intArr1));
        });

        test(`arrayIsEqual(${toStr(intArr1)}, ["1", "2", "3"]) returns false`, function()
        {
            assert.isFalse(arrayIsEqual(intArr1, arr1));
        });
    });
});
