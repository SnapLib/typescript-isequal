/* eslint-disable @typescript-eslint/ban-types */
import {objectIsEqual} from "../../../main/ts/isEqual/objectIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null, Infinity, NaN];

const stringArray = ["first", "second", "third"];
const intArray = [3, 6, 9];
const mixedArray1 = ["first", 2, true, {}, Symbol("aSymbol")];
const mixedArray2 = [false, {foo: 123}, "bar", 456, BigInt(99999)];

const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);
const map2 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));
const map3 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const arrays = [stringArray, intArray, mixedArray1, mixedArray2];
const maps = [map1, map2, map3];

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const nonGenericObjects = [...arrays, new Set(mixedArray2), ...maps, () => "", 123, "foo", BigInt(999999999), Symbol("aSymbol"), ...singletonPrimitives];

const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

suite("objectIsEqual", function testObjectIsEqual()
{
    suite("invalid source object throws", function testInvalidSourceObjectThrows()
    {
        nonGenericObjects.forEach(nonObj =>
            test(`objectIsEqual(${toStr(nonObj)}, {}) throws`, function()
            {
                assert.throws(() => objectIsEqual(nonObj as {}, {}), TypeError);
            })
        );
    });

    suite("valid source object doesn't throw", function testValidSourceObjectDoesNotThrows()
    {
        objects.forEach(obj =>
            test(`objectIsEqual(${toStr(obj)}, {}) doesn't throw`, function()
            {
                assert.doesNotThrow(() => objectIsEqual(obj, {}));
            })
        );
    });

    suite("equality of non object returns false", function testNonObjectReturnsFalse()
    {
        nonGenericObjects.forEach(nonObj =>
            test(`objectIsEqual({}, ${toStr(nonObj)}) returns false`, function()
            {
                assert.isFalse(objectIsEqual({}, nonObj));
            })
        );
    });

    suite("same object returns true", function testSameObjectReturnsTrue()
    {
        objects.forEach(obj =>
            test(`objectIsEqual(${toStr(obj)}, ${toStr(obj)}) returns true`, function()
            {
                assert.isTrue(objectIsEqual(obj, obj));
            })
        );
    });

    suite("equally structured object returns true", function testEquallyStructuredObjectReturnsTrue()
    {
        objects.forEach(obj =>
            test(`objectIsEqual(${toStr(obj)}, {...${toStr(obj)}}) returns true`, function()
            {
                assert.isTrue(objectIsEqual(obj, {...obj}));
            })
        );
    });

    suite("unequal object returns false", function testUnequalObjectReturnsFalse()
    {
        objects.forEach((obj, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`objectIsEqual(${toStr(obj)}, ${toStr(arr[altIndex])}) returns false`, function()
                {
                    assert.isFalse(objectIsEqual(obj, arr[altIndex]));
                });
            }
        });
    });
});
