/* eslint-disable no-empty-function */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/ban-types */
import {functionIsEqual} from "../../../main/ts/isEqual/functionIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null, Infinity, NaN];

const stringArray = ["first", "second", "third"];
const intArray = [3, 6, 9];
const mixedArray1 = ["first", 2, true, {}];
const mixedArray2 = [false, {foo: 123}, "bar", 456];

const arrays = [stringArray, intArray, mixedArray1, mixedArray2];

const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);
const map2 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));
const map3 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const maps = [map1, map2, map3];

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const sets = arrays.map(array => new Set(array));

const nonFunctions = [...objects, ...arrays, ...sets, ...maps, 123, "foo", BigInt(999999999), Symbol("aSymbol"), ...singletonPrimitives];

const arrowFunc1A = (arg) =>
{
    return arg + 1;
};

const arrowFunc1B = (arg) => { return arg+1; };

function namedFunc1A()
{
    return "bleeep" + 3 * 4;
}

function namedFunc1B() {return "bleeep"+3 *4   ;}

const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

const functions = [toStr, arrowFunc1A, arrowFunc1B, namedFunc1A, namedFunc1B];

suite("functionIsEqual", function testFunctionIsEqual()
{
    suite("invalid source function throws", function testInvalidSourceFunctionThrows()
    {
        nonFunctions.forEach(nonFunc =>
            test(`functionIsEqual(${toStr(nonFunc)}, () => {}) throws`, function()
            {
                assert.throws(() => functionIsEqual(nonFunc as unknown as Function, () => {}), TypeError);
            })
        );
    });

    suite("same function returns true", function testSameFunctionReturnsTrue()
    {
        functions.forEach(func =>
            test(`functionIsEqual(${func}, ${func}) returns true`, function()
            {
                assert.isTrue(functionIsEqual(func, func));
            })
        );
    });

    suite("equal function structure returns true", function testEqualFunctionStructureReturnsTrue()
    {
        test("functionIsEqual(() => {}, () => {}) returns true", function()
        {
            assert.isTrue(functionIsEqual(() => {}, () => {}));
        });

        test("functionIsEqual(function() {}, function() {}) returns true", function()
        {
            assert.isTrue(functionIsEqual(function() {}, function() {}));
        });

        test('functionIsEqual(() => "aString", () => "aString") returns true', function()
        {
            assert.isTrue(functionIsEqual(() => "aString", () => "aString"));
        });

        test("functionIsEqual((x) => { return x + 2 }, (x) => {return x+2;}) returns true", function()
        {
            assert.isTrue(functionIsEqual((x) => { return x + 2; }, (x) => {return x+2;}));
        });

        test('functionIsEqual(function() {return "";}, function() {return "";}) returns true', function()
        {
            assert.isTrue(functionIsEqual(function() {return "aString";}, function() {return "aString";}));
        });

        test('functionIsEqual(function() {return "";}, function() {return "";}) returns true', function()
        {
            assert.isTrue(functionIsEqual(function conk() {return "aString";}, function conk() {return "aString";}));
        });

        test("functionIsEqual(function(x) { return x + 2 }, function(x) {return x+2;}) returns true", function()
        {
            assert.isTrue(functionIsEqual(function(x) { return x + 2; }, function(x) {return x+2;}));
        });

        test("functionIsEqual(function foo(x) { return x + 2 }, function foo(x) {return x+2;}) returns true", function()
        {
            assert.isTrue(functionIsEqual(function foo(x) { return x + 2; }, function foo(x) {return x+2;}));
        });
    });

    suite("unequal functions return false", function testUnequalFunctionsReturnFalse()
    {
        functions.forEach((mockFunc, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`functionIsEqual(${toStr(mockFunc)}, ${toStr(arr[altIndex])}) returns false`, function()
                {
                    assert.isFalse(functionIsEqual(mockFunc, arr[altIndex]));
                });
            }
        });
    });

    suite("equality of non-function returns false", function testNonFunctionReturnsFalse()
    {
        nonFunctions.forEach(nonFunc =>
            test(`functionIsEqual(() => {}, ${toStr(nonFunc)}) returns false`, function()
            {
                assert.isFalse(functionIsEqual(() => {}, nonFunc));
            })
        );
    });
});
