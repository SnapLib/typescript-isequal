import {numericIsEqual} from "../../../main/ts/isEqual/numericIsEqual";
import {assert} from "chai";
import {suite, test} from "mocha";

const singletonPrimitives = [true, false, undefined, null];

const stringArray = ["first", "second", "third"];
const intArray = [3, 6, 9];
const mixedArray1 = ["first", 2, true, {}, Symbol("aSymbol")];
const mixedArray2 = [false, {foo: 123}, "bar", 456, BigInt(99999)];

const numbers = [-432, -100, 0, 212, 99999, Infinity, NaN];

const bigInts = numbers.filter(num => Number.isFinite(num)).map(BigInt);

const map1 = new Map([
    ["first", 1],
    ["second", 2],
    ["third", 3]
]);
const map2 = new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]]));
const map3 = new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]]));

const obj1 = {
    prop1: "first",
    prop2: true,
    prop3: 3,
    prop4: mixedArray1,
    prop5: {foo: "bar", baz: ["bang"]},
    prop6: map2,
    prop7: new Set(mixedArray2),
    prop8: null,
    prop9: undefined,
    toString: () => "MockObject"
};

const arrays = [stringArray, intArray, mixedArray1, mixedArray2];
const maps = [map1, map2, map3];

const objs1 = arrays.map(array => Object.fromEntries(array.entries()));
const objs2 = maps.map(map => Object.fromEntries(map.entries()));

const objects = [{}, obj1, ...objs1, ...objs2];

const sets = arrays.map(array => new Set(array));

const nonNumbers = [...objects, ...arrays, ...sets, () => "", "foo", Symbol("aSymbol"), ...singletonPrimitives];

const toStr = (o: unknown): string =>
{
    return typeof o === "string" ? (o.includes('"') ? `'${o}'` : `"${o}"`)
    : typeof o === "symbol" ? `Symbol("${o.description}")`
    : typeof o === "bigint" ? `BigInt(${o})`
    : o instanceof Set ? `Set(${toStr(Array.from(o))})`
    : o instanceof Map ? `{${Array.from(o.entries()).map(entry => `${toStr(entry[0])} => ${toStr(entry[1])}`).join(", ")}}`
    : Array.isArray(o) ? `[${o.map(e => toStr(e)).join(", ")}]`
    : o && Object.entries(o).length !== 0 ? `{${Object.entries(o).map(entry => `${entry[0]}: ${toStr(entry[1])}`).join(", ")}}`
    : `${o}`;
};

suite("numericIsEqual", function testNumericIsEqual()
{
    suite("invalid source numeric throws", function testInvalidSourceNumericThrows()
    {
        nonNumbers.forEach(nonNumber =>
            test(`numericIsEqual(${toStr(nonNumber)}, 42) throws`, function()
            {
                assert.throws(() => numericIsEqual(nonNumber as unknown as number | BigInt, 42), TypeError);
            })
        );
    });

    suite("equality of non-numeric returns false", function testNonNumericReturnsFalse()
    {
        nonNumbers.forEach(nonNumber =>
            test(`numericIsEqual(42, ${toStr(nonNumber)})`, function()
            {
                assert.isFalse(numericIsEqual(42, nonNumber));
            })
        );
    });

    suite("primitive numeric singletons", function testEqualityOfPrimitiveNumericSingletons()
    {
        [Infinity, NaN].forEach(numericPrimitiveSingleton => {
            test(`numericIsEqual(${numericPrimitiveSingleton}, 42) returns false`, function testNumericPrimitiveSingletonAnd42ReturnsFalse()
            {
                assert.isFalse(numericIsEqual(numericPrimitiveSingleton, 42));
            });

            test(`numericIsEqual(42, ${numericPrimitiveSingleton}) returns false`, function test42AndNumericPrimitiveSingletonReturnsFalse()
            {
                assert.isFalse(numericIsEqual(42, numericPrimitiveSingleton));
            });

            test(`numericIsEqual(${numericPrimitiveSingleton}, BigInt(42)) returns false`, function testNumericPrimitiveSingletonAndBigInt42ReturnsFalse()
            {
                assert.isFalse(numericIsEqual(numericPrimitiveSingleton, BigInt(42)));
            });

            test(`numericIsEqual(BigInt(42), ${numericPrimitiveSingleton}) returns false`, function testBigInt42AndNumericPrimitiveSingletonReturnsFalse()
            {
                assert.isFalse(numericIsEqual(BigInt(42), numericPrimitiveSingleton));
            });
        });

        test("numericIsEqual(Infinity, Infinity) returns true", function testInfinityAndInfinityReturnsTrue()
        {
            assert.isTrue(numericIsEqual(Infinity, Infinity));
        });

        test("numericIsEqual(NaN, NaN) returns true", function testNaNAndNaNReturnsTrue()
        {
            assert.isTrue(numericIsEqual(NaN, NaN));
        });
    });

    suite("equal primitive numbers return true", function testEqualPrimitiveNumbersReturnTrue()
    {
        numbers.forEach(number =>
            test(`numericIsEqual(${number}, ${number}) returns true`, function()
            {
                assert.isTrue(numericIsEqual(number, number));
            })
        );
    });

    suite("equal BigInts return true", function testEqualBigIntsReturnTrue()
    {
        bigInts.forEach(bigInt =>
            test(`numericIsEqual(${toStr(bigInt)}, ${toStr(bigInt)}) returns true`, function()
            {
                assert.isTrue(numericIsEqual(bigInt, bigInt));
            })
        );
    });

    suite("unequal primitive numbers return false", function testUnequalPrimitiveNumbersReturnFalse()
    {
        numbers.forEach((number, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`numericIsEqual(${number}, ${arr[altIndex]}) returns false`, function()
                {
                    assert.isFalse(numericIsEqual(number, arr[altIndex]));
                });
            }
        });
    });

    suite("unequal BigInts return false", function testUnequalBigIntsReturnFalse()
    {
        bigInts.forEach((bigInt, index, arr) => {
            const altIndex = arr.length - 1 - index;

            if (index !== altIndex)
            {
                test(`numericIsEqual(${toStr(bigInt)}, ${toStr(arr[altIndex])}) returns false`, function()
                {
                    assert.isFalse(numericIsEqual(bigInt, arr[altIndex]));
                });
            }
        });
    });

    suite("equal primitive number and BigInt returns true", function testEqualPrimitiveNumberAndBigIntReturnsTrue()
    {
        bigInts.forEach((bigInt, index) =>
            test(`numericIsEqual(${numbers[index]}, ${toStr(bigInt)}) returns true`, function()
            {
                assert.isTrue(numericIsEqual(numbers[index], bigInt));
            })
        );
    });

    suite("equal BigInt and primitive number returns true", function testEqualBigIntAndPrimitiveNumberReturnsTrue()
    {
        bigInts.forEach((bigInt, index) =>
            test(`numericIsEqual(${toStr(bigInt)}, ${numbers[index]}) returns true`, function()
            {
                assert.isTrue(numericIsEqual(bigInt, numbers[index]));
            })
        );
    });
});
