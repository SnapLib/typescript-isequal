import {stringArray1, stringArray2, intArray1, intArray2, mixedArray1, mixedArray2} from "./mockArray";

export const emptyMap: ReadonlyMap<undefined, undefined> = Object.freeze(new Map());
export const stringNumberMap1: ReadonlyMap<string, number> = Object.freeze(new Map(stringArray1.map((str, index) => [str, index])));
export const stringNumberMap2: ReadonlyMap<string, number> = Object.freeze(new Map(stringArray2.map((str, index) => [str, index])));
export const intStringMap1: ReadonlyMap<number, string> = Object.freeze(new Map(intArray1.map((num, index) => [num, stringArray1[index]])));
export const intStringMap2: ReadonlyMap<number, string> = Object.freeze(new Map(intArray2.map((num, index) => [num, stringArray2[index]])));
export const mixedMap1: ReadonlyMap<unknown, unknown> = Object.freeze(new Map(mixedArray1.map((element, index) => [element, mixedArray2[index]])));
export const mixedMap2: ReadonlyMap<unknown, unknown> = Object.freeze(new Map(mixedArray2.map((element, index) => [element, mixedArray1[index]])));
export const allMaps: readonly ReadonlyMap<unknown, unknown>[] = Object.freeze([emptyMap, stringNumberMap1, stringNumberMap2, intStringMap1, intStringMap2, mixedMap1, mixedMap2]);

export class MockMap
{
    public static readonly empty: ReadonlyMap<undefined, undefined> = emptyMap;
    public static readonly stringNumber1: ReadonlyMap<string, number> = stringNumberMap1;
    public static readonly stringNumber2: ReadonlyMap<string, number> = stringNumberMap2;
    public static readonly intString1: ReadonlyMap<number, string> = intStringMap1;
    public static readonly intString2: ReadonlyMap<number, string> = intStringMap2;
    public static readonly mixed1: ReadonlyMap<unknown, unknown> = mixedMap1;
    public static readonly mixed2: ReadonlyMap<unknown, unknown> = mixedMap2;
    public static readonly all: readonly ReadonlyMap<unknown, unknown>[] = allMaps;
}

export default MockMap;
