import {MockArray} from "./mockArray";
import {MockMap} from "./mockMap";
import {MockSet} from "./mockSet";
import {strict as assert} from "assert";

export const emptyObject = Object.freeze({});
export const nullObject = Object.freeze(Object.create(null));
export const superObject = Object.freeze({
    trueBool: true,
    falseBool: false,
    undefinedValue: undefined,
    nullValue: null,
    nanValue: NaN,
    positiveInfinity: Infinity,
    negativeInfinity: -Infinity,
    aString: "first",
    aSymbol: Symbol("aSymbol"),
    aPositiveInt: Math.round(Math.random()) * Number.MAX_SAFE_INTEGER,
    aPositiveFloat: Math.random() * Number.MAX_SAFE_INTEGER,
    aPositiveBigInt: BigInt(Math.round(Math.random() * Number.MAX_VALUE)),
    aNegativeInt: Math.round(Math.random()) * Number.MIN_SAFE_INTEGER,
    aNegativeFloat: Math.random() * Number.MIN_SAFE_INTEGER,
    aNegativeBigInt: BigInt(Math.round(Math.random() * Number.MIN_VALUE)),
    emptyObject: emptyObject,
    nullObject: nullObject,
    emptyArray: MockArray.empty,
    arrayOfStrings: MockArray.strings1,
    arrayOfInts: MockArray.ints1,
    arrayOfSymbols: MockArray.symbols1,
    arrayOfMixed: MockArray.mixed1,
    arrayOfArrays: [MockArray.empty, MockArray.primitiveSingletons, MockArray.strings1, MockArray.strings2, MockArray.ints1, MockArray.ints2, MockArray.symbols1, MockArray.symbols2, MockArray.mixed1, MockArray.mixed2],
    arrayOfSets: MockSet.all,
    arrayOfMaps: MockMap.all,
    arrayOfPrimitiveSingletons: MockArray.primitiveSingletons,
    emptySet: MockSet.empty,
    setOfStrings: MockSet.strings1,
    setOfInts: MockSet.ints1,
    setOfSymbols: MockSet.symbols1,
    setOfMixed: MockSet.mixed1,
    setOfArrays: new Set(MockArray.all),
    setOfSets: new Set(MockSet.all),
    setOfMaps: new Set(MockMap.all),
    setOfPrimitiveSingletons: MockSet.primitiveSingletons,
    emptyMap: MockMap.empty,
    stringNumberMap: MockMap.stringNumber1,
    intStringMap: MockMap.intString1,
    mixedMap: MockMap.mixed1,
    toString: (): string => "MockSuperObject"
});

assert.strictEqual(superObject.arrayOfArrays.length - 1, MockArray.all.length);
