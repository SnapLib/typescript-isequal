import {emptyArray, primitiveSingletonsArray, stringArray1, stringArray2, intArray1, intArray2, symbolArray1, symbolArray2, mixedArray1, mixedArray2} from "./mockArray";

export const emptySet: ReadonlySet<undefined> = Object.freeze(new Set(emptyArray));
export const primitiveSingletonsSet: ReadonlySet<boolean | undefined | null | number> = Object.freeze(new Set(primitiveSingletonsArray));
export const stringSet1: ReadonlySet<string> = Object.freeze(new Set(stringArray1));
export const stringSet2: ReadonlySet<string> = Object.freeze(new Set(stringArray2));
export const intSet1: ReadonlySet<number> = Object.freeze(new Set(intArray1));
export const intSet2: ReadonlySet<number> = Object.freeze(new Set(intArray2));
export const symbolSet1: ReadonlySet<symbol> = Object.freeze(new Set(symbolArray1));
export const symbolSet2: ReadonlySet<symbol> = Object.freeze(new Set(symbolArray2));
export const mixedSet1: ReadonlySet<unknown> = Object.freeze(new Set(mixedArray1));
export const mixedSet2: ReadonlySet<unknown> = Object.freeze(new Set(mixedArray2));
export const allSets: readonly ReadonlySet<unknown>[] = Object.freeze([emptySet, primitiveSingletonsSet, stringSet1, stringSet2, intSet1, intSet2, symbolSet1, symbolSet2, mixedSet1, mixedSet2]);

export class MockSet
{
    public static readonly empty: ReadonlySet<undefined> = emptySet;
    public static readonly primitiveSingletons: ReadonlySet<boolean | undefined | null | number> = primitiveSingletonsSet;
    public static readonly strings1: ReadonlySet<string> = stringSet1;
    public static readonly strings2: ReadonlySet<string> = stringSet2;
    public static readonly ints1: ReadonlySet<number> = intSet1;
    public static readonly ints2: ReadonlySet<number> = intSet2;
    public static readonly symbols1: ReadonlySet<symbol> = symbolSet1;
    public static readonly symbols2: ReadonlySet<symbol> = symbolSet2;
    public static readonly mixed1: ReadonlySet<unknown> = mixedSet1;
    public static readonly mixed2: ReadonlySet<unknown> = mixedSet2;
    public static readonly all: readonly ReadonlySet<unknown>[] = allSets;
}

export default MockSet;
