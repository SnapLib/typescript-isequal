export const emptyArray: ReadonlyArray<undefined> = Object.freeze([]);
export const primitiveSingletonsArray: ReadonlyArray<boolean | undefined | null | number> = Object.freeze([true, false, undefined, null, Infinity, NaN]);
export const stringArray1: ReadonlyArray<string> = Object.freeze(["first", "second", "third"]);
export const stringArray2: ReadonlyArray<string> = Object.freeze(["1", "2", "3"]);
export const intArray1: ReadonlyArray<number> = Object.freeze([1, 2, 3]);
export const intArray2: ReadonlyArray<number> = Object.freeze([9, 8, 7]);
export const symbolArray1: ReadonlyArray<symbol> = Object.freeze([Symbol("firstSymbol"), Symbol("secondSymbol"), Symbol("thirdSymbol")]);
export const symbolArray2: ReadonlyArray<symbol> = Object.freeze([Symbol("fourthSymbol"), Symbol("fifthSymbol"), Symbol("sixthSymbol")]);
export const mixedArray1: ReadonlyArray<unknown> = Object.freeze(["first", 2, undefined, true, {}, Symbol("aSymbol"), null]);
export const mixedArray2: ReadonlyArray<unknown> = Object.freeze([false, {foo: 123}, new Set(stringArray1), "bar", 456, BigInt(99999), Symbol("foobar")]);
export const allArrays: readonly ReadonlyArray<unknown>[] = Object.freeze([emptyArray, primitiveSingletonsArray, stringArray1, stringArray2, intArray1, intArray2, symbolArray1, symbolArray2, mixedArray1, mixedArray2]);

export class MockArray
{
    public static readonly empty: ReadonlyArray<undefined> = emptyArray;
    public static readonly primitiveSingletons: ReadonlyArray<boolean | undefined | null | number> = primitiveSingletonsArray;
    public static readonly strings1: ReadonlyArray<string> = stringArray1;
    public static readonly strings2: ReadonlyArray<string> = stringArray2;
    public static readonly ints1: ReadonlyArray<number> = intArray1;
    public static readonly ints2: ReadonlyArray<number> = intArray2;
    public static readonly symbols1: ReadonlyArray<symbol> = symbolArray1;
    public static readonly symbols2: ReadonlyArray<symbol> = symbolArray2;
    public static readonly mixed1: ReadonlyArray<unknown> = mixedArray1;
    public static readonly mixed2: ReadonlyArray<unknown> = mixedArray2;
    public static readonly all: readonly ReadonlyArray<unknown>[] = allArrays;
}

export default MockArray;
