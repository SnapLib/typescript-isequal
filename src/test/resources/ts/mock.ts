import {MockArray} from "./mock/mockArray";
import {MockMap} from "./mock/mockMap";
import {MockSet} from "./mock/mockSet";

export class Mock
{
    public static readonly array: Readonly<MockArray> = Object.freeze(MockArray);
    public static readonly map: Readonly<MockMap> = Object.freeze(MockMap);
    public static readonly set: Readonly<MockSet> = Object.freeze(MockSet);
}

export * from "./mock/mockArray";
export * from "./mock/mockMap";
export * from "./mock/mockSet";
export default Mock;
