import {isEqual} from "../isEqual";

export const objectIsEqual = (obj: {[key: string]: unknown}, arg: unknown): boolean =>
{
    if (typeof obj !== "object" || obj === null || Array.isArray(obj) || obj instanceof Set  || obj instanceof Map)
    {
        throw new TypeError("illegal source object type");
    }

    if (obj === arg)
    {
        return true;
    }

    if (obj === null)
    {
        return arg === null;
    }

    if (typeof arg !== "object" || Array.isArray(arg) || arg instanceof Set  || arg instanceof Map)
    {
        return false;
    }

    if (arg === null)
    {
        return obj === null;
    }

    const objEntries: readonly Readonly<[string, unknown]>[] =
        Object.freeze(Object.entries(obj));

    const argEntries: readonly Readonly<[string, unknown]>[] =
        Object.freeze(Object.entries(arg));

    if (objEntries.length !== argEntries.length)
    {
        return false;
    }

    if ( ! objEntries.every(entry => entry[0] in arg) || ! argEntries.every(entry => entry[0] in obj))
    {
        return false;
    }

    return objEntries.every(entry => argEntries.some(argEntry => entry[0] === argEntry[0] && isEqual(entry[1], argEntry[1])));
};

export default objectIsEqual;
