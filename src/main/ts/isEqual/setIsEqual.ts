import {isEqual} from "../isEqual";

export const setIsEqual = (set: Set<unknown>, arg: unknown): boolean =>
{
    if ( ! (set instanceof Set))
    {
        throw new TypeError("illegal source set type");
    }

    if (set === arg)
    {
        return true;
    }

    if ( ! (arg instanceof Set))
    {
        return false;
    }

    if (set.size !== arg.size)
    {
        return false;
    }

    const argArray = Object.freeze(Array.from(arg));

    return Array.from(set).every(element => argArray.some(argElement => isEqual(element, argElement)));
};

export default setIsEqual;
