import {isEqual} from "../isEqual";

export const arrayIsEqual = (arr: Array<unknown>, arg: unknown): boolean =>
{
    if ( ! Array.isArray(arr))
    {
        throw new TypeError("illegal source array type");
    }

    if (arr === arg)
    {
        return true;
    }

    if ( ! Array.isArray(arg))
    {
        return false;
    }

    if (arr.length !== arg.length)
    {
        return false;
    }

    return arr.every((element, index) => isEqual(element, arg[index]));
};

export default arrayIsEqual;
