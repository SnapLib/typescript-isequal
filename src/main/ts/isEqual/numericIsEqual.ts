import {numberIsEqual} from "./numericIsEqual/numberIsEqual";
import {bigIntIsEqual} from "./numericIsEqual/bigIntIsEqual";

export const numericIsEqual = (numeric: number | BigInt, arg: unknown): boolean =>
{
    if (typeof numeric !== "number" && typeof numeric !== "bigint")
    {
        throw new TypeError("illegal source numeric type");
    }

    if (numeric === arg)
    {
        return true;
    }

    if (typeof arg !== "number" && typeof arg !== "bigint")
    {
        return false;
    }

    if (typeof numeric === "number")
    {
        return numberIsEqual(numeric, arg);
    }

    if (typeof numeric === "bigint")
    {
        return bigIntIsEqual(numeric, arg);
    }

    throw new Error("unparsable numeric argument");
};

export {numberIsEqual} from "./numericIsEqual/numberIsEqual";
export {bigIntIsEqual} from "./numericIsEqual/bigIntIsEqual";

export default numericIsEqual;
