import {isEqual} from "../isEqual";

export const mapIsEqual = (map: Map<unknown, unknown>, arg: unknown): boolean =>
{
    if ( ! (map instanceof Map))
    {
        throw new TypeError("illegal source map type");
    }

    if (map === arg)
    {
        return true;
    }

    if ( ! (arg instanceof Map))
    {
        return false;
    }

    if (map.size !== arg.size)
    {
        return false;
    }

    const srcMapEntries = Object.freeze(Array.from(map.entries()));

    if ( ! srcMapEntries.every(srcMapEntry => arg.has(srcMapEntry[0])))
    {
        return false;
    }

    return srcMapEntries.every(srcMapEntry => isEqual(srcMapEntry[1], arg.get(srcMapEntry[0])));
};

export default mapIsEqual;
