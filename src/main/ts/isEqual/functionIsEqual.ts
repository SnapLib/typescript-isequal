// eslint-disable-next-line @typescript-eslint/ban-types
export const functionIsEqual = (func: Function, arg: unknown): boolean =>
{
    if (typeof func !== "function")
    {
        throw new TypeError("illegal source function type");
    }

    if (func === arg)
    {
        return true;
    }

    if (typeof arg !== "function")
    {
        return false;
    }

    if (func.length !== arg.length)
    {
        return false;
    }

    if (func.name !== arg.name)
    {
        return false;
    }

    const formattedFuncString = func.toString().replace(/s+/g, "");
    const formattedArgFuncString = arg.toString().replace(/s+/g, "");

    return formattedFuncString === formattedArgFuncString;
};

export default functionIsEqual;
