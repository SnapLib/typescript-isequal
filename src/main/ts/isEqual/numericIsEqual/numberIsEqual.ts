export const numberIsEqual = (num: number, arg: unknown): boolean =>
{
    if (typeof num !== "number")
    {
        throw new TypeError("illegal source number type");
    }

    if (num === arg)
    {
        return true;
    }

    if (typeof arg !== "number" && typeof arg !== "bigint")
    {
        return false;
    }

    if (typeof arg === "number")
    {
        if (Number.isNaN(num) && Number.isNaN(arg))
        {
            return true;
        }

        if (num === Infinity && arg === Infinity)
        {
            return true;
        }
    }

    if (typeof arg === "bigint")
    {
        if ( ! Number.isInteger(num))
        {
            return false;
        }

        return BigInt(num) === arg;
    }

    return num === arg;
};

export default numberIsEqual;
