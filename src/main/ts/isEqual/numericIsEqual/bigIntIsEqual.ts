export const bigIntIsEqual = (bigInt: BigInt, arg: unknown): boolean =>
{
    if (typeof bigInt !== "bigint")
    {
        throw new TypeError("illegal source BigInt type");
    }

    if (bigInt === arg)
    {
        return true;
    }

    if (typeof arg !== "bigint" && typeof arg !== "number")
    {
        return false;
    }

    if (typeof arg === "number")
    {
        if (Number.isNaN(arg) || arg === Infinity || ! Number.isInteger(arg))
        {
            return false;
        }

        return bigInt === BigInt(arg);
    }

    return bigInt === arg;
};

export default bigIntIsEqual;
