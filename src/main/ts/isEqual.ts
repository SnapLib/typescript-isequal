import {arrayIsEqual} from "./isEqual/arrayIsEqual";
import {functionIsEqual} from "./isEqual/functionIsEqual";
import {mapIsEqual} from "./isEqual/mapIsEqual";
import {numericIsEqual} from "./isEqual/numericIsEqual";
import {objectIsEqual} from "./isEqual/objectIsEqual";
import {setIsEqual} from "./isEqual/setIsEqual";

export const isEqual = (source: unknown, target: unknown): boolean =>
{
    if (source === target)
    {
        return true;
    }

    if (Array.isArray(source))
    {
        return arrayIsEqual(source, target);
    }

    if (typeof source === "function")
    {
        return functionIsEqual(source, target);
    }

    if (source instanceof Map)
    {
        return mapIsEqual(source, target);
    }

    if (typeof source === "number" || typeof source === "bigint")
    {
        return numericIsEqual(source, target);
    }

    if (source instanceof Set)
    {
        return setIsEqual(source, target);
    }

    if (typeof source === "object" && source !== null)
    {
        return objectIsEqual(source as {[key: string]: unknown}, target);
    }

    return source === target;
};

export {arrayIsEqual} from "./isEqual/arrayIsEqual";
export {functionIsEqual} from "./isEqual/functionIsEqual";
export {mapIsEqual} from "./isEqual/mapIsEqual";
export {numericIsEqual, numberIsEqual, bigIntIsEqual} from "./isEqual/numericIsEqual";
export {objectIsEqual} from "./isEqual/objectIsEqual";
export {setIsEqual} from "./isEqual/setIsEqual";

export default isEqual;
