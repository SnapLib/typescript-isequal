# isEqual

Check elements for *equality*, not sameness.

![npm (scoped)][1] ![NPM][2] ![node-current (scoped)][3]

This npm package exports the `isEqual` function as well as all it's sub
functions.

[1]: https://img.shields.io/npm/v/@snaplib/is-equal?color=%2366ff66&logo=npm&style=flat-square
[2]: https://img.shields.io/npm/l/@snaplib/is-equal?color=%2366ff66&style=flat-square
[3]: https://img.shields.io/node/v/@snaplib/is-equal?color=%2366ff66&&logo=node.js&style=flat-square
